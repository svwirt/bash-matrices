#!/bin/bash

##################################################################################################################
# Stephanie Ayala
# 07/08/2018
# Program 1 - Matrices
# Description: this program reads matrices from files or stdin. For one matrix, it computes the dimensions
# transposes the matrix, and finds the mean value of each collunm.  It also adds and multiplies two matrices.
##################################################################################################################

# datapathfiles for stdin input
datafilepath="datafile$$"
datafilepath2="datafile2$$"

# this function helps the trap remove all files
cleanFiles(){
  # remove all files that begin with datafile or temp
  rm -f datafile* temp*
  exit 
}

# The trap catches if the program is terminated or hung
trap cleanFiles INT HUP TERM EXIT

# This function helps get the dimensions for the dims function. 
# it stores the dimensions in a file
dimsHelper(){
    dimA="tempdimfileA$$"
    rowCount=0

    while read matrixLine
    do
        collumnCount=0
        # Outer while loop counts rows
        rowCount=`expr $rowCount + 1`

        for i in $matrixLine
        do
            # inner for loop counts the collumns
            collumnCount=`expr $collumnCount + 1`
        done
    done < $1
    echo -n "$rowCount " > $dimA$$
    echo "$collumnCount" >> $dimA$$
}

# This function prints the dimensions of the matrix 
dims(){
    # accepts only one parameter
    if [ "$#" -ne 1 ]
    then
        echo "Illegal number of parameters" 1>&2
        exit 1
    fi
    if [[ ! -r $datafilepath ]]
    then
        echo "File not found" 1>&2
        exit 1
    fi
        # uses helper function to put dimensions in file
        dimsHelper $datafilepath
        # print that file
        cat $dimA$$
        rm $dimA$$

    exit
}

# this helper function stores the transposed matrix in a file
helper(){
    tempRow="temprowfile$$"
    tempColl="tempcolfile$$"
    tempTrans="temptransfile$$"
    tempX="tempfile1$$"
    tempY="tempfile2$$"

    # get column number
    # put the dimensions of the matrix in a file
    dimsHelper $1
    mycount=0
    # put the dimensions into variables
    while read myLine
    do
        for i in $myLine
        do 
            if [ "$mycount" -eq 0 ]
            then
                mycount=1            
            else
                # index gets the number of columns
                index=$i
            fi
        done
    done<$dimA$$
    
   
    i=1
    # while i in the matrix is less than or equal to the number of columns  
    while [[ "$i" -le "$index" ]]
    do
        # cut first number (any number of characters) from the matrix and put it into tempColumn file
        cut -f $i $1 > $tempColl$$
        
        # pipe the file into temp row and reverse the newlines with tabs
      	cat $tempColl$$ | tr -s '\n' '\t' > "$tempRow$$"
        # https://en.wikipedia.org/wiki/Tr_(Unix)
        
        # reverse temprow into tempX 
        rev "$tempRow$$" >$tempX$$
        # https://www.cyberciti.biz/faq/how-to-reverse-string-in-unix-shell-script/
        
        # https://stackoverflow.com/questions/13690461/using-cut-command-to-remove-multiple-columns
        cat $tempX$$ | cut -c 2-  >"$tempY$$"
        # reverse int o tempRow
        rev "$tempY$$">$tempRow$$
        cat $tempRow$$ >> $tempTrans$$
        #increment i
      	(( i++ ))
    done
    rm $tempRow$$
    rm $tempColl$$
    rm $tempX$$
    rm $tempY$$
}

# this function calls the helper function for transpose and prints the results that were stored
transpose(){
    # Accepts only one parameter
    if [ "$#" -ne 1 ]
    then
        echo "Illegal number of parameters" 1>&2
        exit 1
    else
      # if the file is there and readable
      if [ -r "$datafilepath" -a -f "$datafilepath" ]
      then
          # use helper function to store transposed matrix
          helper $datafilepath
          # print from the file that stored transposed matrix
          cat $tempTrans$$
          rm $tempTrans$$
      else
          echo "file not readable" 1>&2
          exit 1
      fi
    fi
    exit
}


# This function accepts one parameter (a matrix file) If it is readable, it will find the mean 
# of the columns in the matrix.  
mean(){
    # if there is more than one parameter fiele, an error will be thrown and it will exit with a 1
    if [ "$#" -ne 1 ]
    then
        echo "Illegal number of parameters" 1>&2
        exit 1
    fi
    # throw an error if the file is not readable
    if [[ ! -r $datafilepath ]]
    then
        echo "File not found" 1>&2
        exit 1
    else

          tempMean="tempMeanFile$$"
          meanFile="tempsmeanfile$$"
          tempB="tempbfile$$"
          lineNum=0

          # transpose the matrix and store in a file
          helper $datafilepath

          while read myLine
          do
              sum=0
              count=0
              average=0
              # keep track of line number for matrix
              lineNum=`expr $lineNum + 1`

              for i in $myLine
              do
                  num=$i
                  # keep track of count for i in line
                  count=`expr $count + 1`
                  sum=`expr $sum + $num`
                done

              # formula given in homework description
              average=$(((sum + (count/2)*( (sum>0)*2-1 )) / count))
              # (a + (b/2)*( (a>0)*2-1 )) / b
              echo -e  "$average" >> $tempMean$$
              cat $tempMean$$ | tr '\n' '\t' > "$meanFile$$"

        done <$tempTrans$$
        echo -e "" >> $meanFile$$

        # trim off the extra white space
        myvar=$(cat $meanFile$$)
        echo "${myvar::-1}"
        # http://masteringunixshell.net/qa45/bash-how-to-trim-string.html

        rm  $meanFile$$
        rm  $tempMean$$
        rm  $tempTrans$$

    fi
    exit
}


# this function must have two matrix files as inputs.  If there are two files that are readable then the matrices
# must have the same dimensions.  If the matrices meet all these requirments, then they will be added together and 
# the resulting matrix will be printed.
add(){
    # if there are more than two arguments it will throw an error and exit
    if [ "$#" -ne 2 ]
    then
        echo "Illegal number of parameters" 1>&2
        exit 1
    else
           # if both datafilepaths are readable
          if [ -r "$datafilepath" -a -f "$datafilepath" ] && [ -r "$datafilepath2" -a -f "$datafilepath2" ]
          then
              tempA="tempfileA$$"
              mycount=0
              count1=0

              # put the dimensions of the first matrix in a file
              dimsHelper $datafilepath

              # put the dimensions into variables
              while read myLine
              do
                  for i in $myLine
                  do
                      if [ "$mycount" -eq 0 ]
                      then
                          # head1 gets the number of rows 
                          head1=$i
                          mycount=1

                      else
                          # tail1 gets the number of columns
                          tail1=$i
                      fi
                  done
              done<$dimA$$

              # put the dimensions of the second matrix in a file
              dimsHelper $datafilepath2

              # put the dimensions into variables
              while read myLine
              do
                  for i in $myLine
                  do
                      if [ "$count1" -eq 0 ]
                      then
                          # head2 gets the number of rows 
                          head2=$i
                          count1=1
                      else
                          # tail2 gets the number of columns
                          tail2=$i
                      fi
                  done
              done< $dimA$$

              # if the matrices are the same size
              if [ "$head1" -eq "$head2" ] && [ "$tail1" -eq "$tail2" ]
              then
              tempA="tempfilea$$"
              tempB="tempfileB$$"

                  # while the lines of both files are being read
                  while
                      read m1_line 0<&3
                      read m2_line 0<&4
                  do
                      # set the count for the outer for loop
                      count=0

                      for i in $m1_line
                      do
                          # set the position for the inner for loop
                          pos=1
                          num=$i
                          # incriment the count each time the outer for loop 
                          count=`expr $count + 1`

                          for j in $m2_line
                          do
                              num2=$j
                              # if the position in each for loop is equal, get the sum of the numbers 
                              # from each for loop
                              if [ "$pos" -eq "$count" ]
                              then
                                  sum=`expr $num + $num2`
                                  # read the sum into a file
                                  echo -n -e "$sum\t" >> $tempA$$
                              fi
                              # increment the position
                              pos=`expr $pos + 1`
                          done
                      done

                      # cut the tab off the end of each line
                      myvar=$(cat $tempA$$)
                      echo "${myvar::-1}"
                      # http://masteringunixshell.net/qa45/bash-how-to-trim-string.html

                      # remove the tempfile to get ready for the next line
                      rm $tempA$$
                  done 3<$datafilepath 4<$datafilepath2
                  rm $dimA$$
              # otherwise the matrices cannot be multiplied
              else
              		echo "incompatable matricies" 1>&2
              		exit 1
              fi
          else
    	       echo "File cannot be read" 1>&2
    	       exit 1
      fi
fi
exit
}


# This function accepts two matrix parameters, if the number fo columns of the first matrix matches the number of 
# rows of the second matrix, then the two matrices will be multiplies
multiply(){
    # if there are more than two arguments it will throw an error and exit
    if [ "$#" -ne 2 ]
    then
        echo "Illegal number of parameters" 1>&2
        exit 1
    else
        # if both datafilepaths are readable
        if [ -r "$datafilepath" -a -f "$datafilepath" ] && [ -r "$datafilepath2" -a -f "$datafilepath2" ]
        then
            tempA="tempfileA$$"
            count1=0
            mycount=0

            # put the dimensions of the first matrix in a file
            dimsHelper $datafilepath

            # put the dimensions into variables
            while read myLine
            do
                for i in $myLine
                do 
                    if [ "$mycount" -eq 0 ]
                    then
                        # head1 gets the number of rows 
                        head1=$i
                        mycount=1
                        
                    else
                        # tail1 gets the number of columns
                        tail1=$i
                    fi
                done
            done<$dimA$$

            # put the dimensions of the second matrix in a file
            dimsHelper $datafilepath2

            # put the dimensions into variables
            while read myLine
            do
                for i in $myLine
                do
                    if [ "$count1" -eq 0 ]
                    then
                        # head2 gets the number of rows 
                        head2=$i
                        count1=1
                    else
                        # tail2 gets the number of columns
                        tail2=$i
                    fi
                done
            done< $dimA$$

            # use the transpose helper to transpose the second matrix
            # this will be used as the input for the second matrix
            helper $datafilepath2

            # if the collumns of the first matrix dose not match the rows of the second
            # then they are not compatable and an error is thrown
            if [ "$tail1" -ne "$head2" ]
            then
            echo "matrices not compatable" 1>&2
                exit 1
            else

                while read m1_line   
                do
                    total=0
                    index=1
                    # for every line in the first matrix, it will read through each line in the second
                    # because it was transposed, it is like it is reading through each column
                    while read m2_line
                    do  

                        for i in $m1_line
                        do
                            num=$i
                            # counter that keeps track of the position of i in the first matrix line
                            count=`expr $count + 1`


                            for j in $m2_line
                            do
                                # counter that keeps trrack of the position of i in the second matrix line
                                num2=$j
                                (( pos++ ))
                                #if the index is the same in both matrix one and matrix two, then we can multiply and
                                # add to the total that will be recorded
                                if [ "$pos" -eq "$count" ]
                                then
                                  
                                    mul=`expr $num \* $num2`
                                    # total for the row x column
                                    total=`expr $total + $mul`
                                    pos=0
                                    # if we found where the pos equals count, then we can break out of the inner for loop
                                    break
                                    # http://tldp.org/LDP/abs/html/loopcontrol.html
                                fi
                            done
                        done     
                        # when the total of the entire row x column is complete, it is read into a file
                        echo -n -e "$total\t" >> $tempA$$
                        # everythin resets to zero for each line
                        index=0
                        total=0
                        pos=0
                        count=0
                    # this matrix is reading from the file that stores the transposed second matrix
                    done<$tempTrans$$
                    # removing the extra tab at the end of the line
                    myvar=$(cat $tempA$$)
                    echo "${myvar::-1}"
                    # http://masteringunixshell.net/qa45/bash-how-to-trim-string.html

                    # delete file so it can be used for the next line in the first matrix
                    rm $tempA$$
                done<$datafilepath
                rm $dimA$$
            fi
            else
                echo "File not readable" 1>&2
                exit 1
        fi
    fi
    # return zero if function succeeds
    exit
}


# if there is only one argument (no files) then input will be transfered to stdin
if [ "$#" = "1" ]
then
  cat > "$datafilepath"
  $1 "$datafiepath ${@:2}"
# if there are two arguments (one file) then that file will be datafilepath
elif [ "$#" = "2" ]
then
    datafilepath=$2

    $1 "${@:2}"
 # if there are three arguments (two files) then thoser files will be datapathfiles   
elif [ "$#" = "3" ]
then
    datafilepath=$2
    datafilepath2=$3
    $1 "${@:2}"
    $2 "${@:3}"
fi



